﻿using System.Threading;
using System.Threading.Tasks;
using ISP.Scale.Server.Accessors;
using ISP.Scale.Server.Managers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ISP.Scale.Server
{
    public class WindowsBackgroundService : BackgroundService
    {
        private readonly IDbLog _dbLogger;
        private readonly IDeviceHub _deviceHub;
        private readonly int _saveToDbInterval;

        public WindowsBackgroundService(IDbLog dbLogger, IDeviceHub deviceHub, IConfiguration configuration)
        {
            _dbLogger = dbLogger;
            _dbLogger.Log("Service Start", LogLevel.Information);
            _deviceHub = deviceHub;
            _saveToDbInterval = configuration.GetSection("SaveToDBInterval_ms").Get<int>();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _deviceHub.SaveToRepository();
                await Task.Delay(_saveToDbInterval, stoppingToken);
            }
        }
    }
}