﻿using System.Threading.Tasks;

namespace ISP.Scale.Server.Managers
{
    public interface IDeviceHub
    {
        void SaveToRepository();
    }
}