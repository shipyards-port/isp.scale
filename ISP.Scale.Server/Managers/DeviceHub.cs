﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ISP.Scale.Server.Accessors;
using ISP.Scale.Server.Engines;
using ISP.Scale.Server.CrossCutting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ISP.Scale.Server.Managers
{
    public class DeviceHub : IDeviceHub
    {

        #region Members

        private readonly IConfiguration _configuration;
        private readonly IWeightEngine _weightEngine;
        private readonly IServiceProvider _provider;
        private readonly IDbLog _dbLogger;
        private readonly IEnumerable<IDevice> _comDevices;

        #endregion Members

        #region Properties


        #endregion Properties

        #region Constructor

        public DeviceHub(IConfiguration configuration, IWeightEngine weightProtocol, IServiceProvider provider, IDbLog dbLogger)
        {
            _configuration = configuration;
            _weightEngine = weightProtocol;
            _provider = provider;
            _dbLogger = dbLogger;
            WeightMessageFactory.SetMesssageType(configuration.GetSection("WeightMessageType").Get<int>());
            _comDevices = CreateTopology();
            _weightEngine.SubscribeToEvents(_comDevices);
            _dbLogger.Log($"{nameof(DeviceHub)} created", LogLevel.Information);
        }

        #endregion Constructor

        #region Public Methods

        public void SaveToRepository()
        {
            _weightEngine.SaveToRepository();
        }

        #endregion Public Methods

        #region Private Methods

        private IEnumerable<IDevice> CreateTopology()
        {
            var comDevices = new List<IDevice>();
            
            var comPortsDevices = _configuration.GetSection("comPortsDevices").Get<Dictionary<string, object>[]>();

            foreach (var comPortsDevice in comPortsDevices)
            {
                var scaleId = (string)comPortsDevice["ScaleId"];
                var head1Port = (string)comPortsDevice["Head1Port"];
                var head2Port = (string)comPortsDevice["Head2Port"];

                var comDevice1 = new ComDevice(scaleId, head1Port, _provider);
                var comDevice2 = new ComDevice(scaleId, head2Port, _provider);

                comDevices.Add(comDevice1);
                comDevices.Add(comDevice2);
            }
            
            _dbLogger.Log($"{nameof(DeviceHub)} create topology with {comDevices.Count} COM devices", LogLevel.Debug);

            return comDevices;
        }

        #endregion Private Methods
    }
}