﻿using System;

namespace ISP.Scale.Server.Enums
{
    [Flags]
    public enum WeightStatus : byte
    {
        Normal = 1, // 0=Normal 1=No weight display
        GrossNet = 2, // 0=Gross 1=Net
        AutoZero = 4, // 0=No Auto zero 1=Auto zero
        Range = 8, // 0=Within range 1=Out of range
        Standstill = 16,// 0=No standstill 1=Standstill
        Minimum = 32, // 0=Normal 1=Under minimum weighing range
        Const = 64, // Always 1
        Parity = 128, // Zero or parity
    }
}