﻿using System;
using System.Collections.Generic;
using ISP.Scale.Server.Enums;

namespace ISP.Scale.Server.CrossCutting
{
    public record WeightMessage(string ScaleId, WeightStatus Status, double Weight, string Port, DateTime ReceivedTime);

    public static class WeightMessageFactory
    {
        // message may vary in different scales
        public static void SetMesssageType(int messageType)
        {
            _messageType = messageType;
            MessageCharCount = _messageType == 2 ? 14 : 8;
        }
        static int _messageType;
        public static int MessageCharCount = 8;

        public static WeightMessage FromString(string scaleId, string port, string msg, DateTime receivedTime)
        {
            if (_messageType == 2)
                return FromString2(scaleId, port, msg, receivedTime);

            return FromString1(scaleId, port, msg, receivedTime);
        }

        // VT300 Data Block
        // Byte 1 - Weight status -  
        // Byte 2 - Polarity - +/-
        // Byte 3-8 - Weight 6 digits, including decimal point if any
        // Byte 9 - Sync - Carriage return (0D hex) for end of transmission
        public static WeightMessage FromString1(string scaleId, string port, string msg, DateTime receivedTime)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));

            if (msg.Length != MessageCharCount)
                throw new ArgumentException($"Input length {msg.Length} != {MessageCharCount}");

            var status = Convert.ToInt32(msg[0]);

            var polarity = msg[1] == '+' ? 1 : -1;
            var weightStr = msg.Substring(2, 6);
            if(double.TryParse(weightStr, out var weight))
                weight *= polarity;
            else
            {
                throw new ArgumentException($"Cant cast {weightStr} to double");
            }

            return new WeightMessage(scaleId, (WeightStatus)status, weight, port, receivedTime);
        }

        // VT300 Data Block
        // Bytes 1-2  - A1
        // Byte 3 - Weight status -  
        // Byte 4 - Polarity - +/-
        // Byte 5-6 - 2 spaces
        // Byte 7-12 - Weight 6 digits, including decimal point if any
        // Bytes 13-14  - Kg
        // Byte 15 - Sync - Carriage return (0D hex) for end of transmission
        public static WeightMessage FromString2(string scaleId, string port, string msg, DateTime receivedTime)
        {
            if (msg == null) throw new ArgumentNullException(nameof(msg));

            if (msg.Length != MessageCharCount)
                throw new ArgumentException($"Input length {msg.Length} != {MessageCharCount}");

            var status = Convert.ToInt32(msg[2]);

            var polarity = msg[3] == '+' ? 1 : -1;
            var weightStr = msg.Substring(6, 6);
            if (double.TryParse(weightStr, out var weight))
                weight *= polarity;
            else
            {
                throw new ArgumentException($"Cant cast {weightStr} to double");
            }

            return new WeightMessage(scaleId, (WeightStatus)status, weight, port, receivedTime);
        }
    }
}