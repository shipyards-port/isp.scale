﻿using System;
using System.Text;

namespace ISP.Scale.Server.CrossCutting
{
    public record Scale(string ScaleId, HeadScale[] HeadScales)
    {
        public readonly object LockObj = new object();

        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var headScale in HeadScales)
            {
                sb.AppendLine($"{headScale}");
            }

            return sb.ToString();
        }
    }

    public record HeadScale(string ScaleId, string Name, string Port)
    {
        public double HeadWeight { get; set; }
        public DateTime HeadReceivedTime { get; set; }

    }
}