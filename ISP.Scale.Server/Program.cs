using ISP.Scale.Server;
using ISP.Scale.Server.Accessors;
using ISP.Scale.Server.Engines;
using ISP.Scale.Server.Managers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Web;

using IHost host = Host.CreateDefaultBuilder(args)
    .UseWindowsService(options =>
    {
        options.ServiceName = "Weight Service";
    })
    .ConfigureLogging(builder =>
    {
        builder.ClearProviders();
        builder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
        builder.AddNLog("NLog.config");
    })
    .UseNLog()
    .ConfigureServices((hostContext, services) =>
    {
        services.AddHostedService<WindowsBackgroundService>()
            .AddSingleton<IDbLog, LogToDBLog>()
            .AddSingleton<IDeviceHub, DeviceHub>()
            .AddSingleton<IWeightEngine, WeightEngine>()
            .AddSingleton<IRepositoryAdapter, DbRepositoryAdapter>();
    })
    .Build();

await host.RunAsync();
