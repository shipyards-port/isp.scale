﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ISP.Scale.Server.Accessors
{
    public class LogToDBLog : IDbLog
    {
        #region Members

        private readonly ILogger<LogToDBLog> _logger;
        private readonly SqlConnection _connection;
        private const int StackTraceMaxDepth = 8;

        private const string LogLevelError = "ERROR";
        private const string LogLevelDebug = "DEBUG";
        private const string LogLevelInfo = "INFO";
        private const string LogLevelExternalProblem = "PROBLEM";

        private const string Query = "insert into work_ErrorsLog " +
                                     "(datetime, error_msg, context, StackTrace, MachineName, UserName, BuildDate, Version, Level) values " +
                                     "(@dt, @msg, @ctxt, @trace, @comp, @user, @bd, @ver, @level);";

        #endregion Members

        #region Constructor

        public LogToDBLog(IConfiguration configuration, ILogger<LogToDBLog> logger)
        {
            _logger = logger;
            var connectionString = configuration.GetConnectionString("LogConnection");
            _logger.LogInformation($"Try connect with with params {connectionString}");
            _connection = new SqlConnection(connectionString);
            try
            {
                _connection.Open();
                _logger.LogInformation($"Connected");
            }
            catch (Exception e)
            {
                _logger.LogWarning(e, "Can not connect to remote logger");
            }
        }

        #endregion Constructor

        #region Public Methods

        public void Log(Exception ex, string error, LogLevel level)
        {
            try
            {
                Log($"{Message(ex)}, {error}", level);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Fail Log To Db");
            }
        }

        public void Log(string error, LogLevel level)
        {
            _logger.Log(level, error);
            // remove log to db because it increase the db size and it is not used 
            //try
            //{
            //    if (level > LogLevel.Debug)
            //        AddToDbLog(error, level);
            //}
            //catch (Exception ex)
            //{
            //    _logger.LogError(ex, "Fail Log To Db");
            //}
        }

        #endregion Public Methods

        #region Private Methods

        private void AddToDbLog(string error, LogLevel level)
        {
            try
            {
                if (_connection.State == ConnectionState.Broken || _connection.State == ConnectionState.Closed)
                {
                    _logger.LogInformation($"Connection state={_connection.State}, close");
                    _connection.Close();
                    _logger.LogInformation($"Connection state={_connection.State}, try reopen");
                    _connection.OpenAsync();
                }

                var version = Assembly.GetEntryAssembly().GetName().Version;
                var buildDate = new DateTime(2000, 1, 1).AddDays(version.Build).AddSeconds(version.Revision * 2);

                var st = new StackTrace();
                var stackTrace = new StringBuilder();
                for (int i = 1, depth = 0; i < st.FrameCount && depth < StackTraceMaxDepth; i++, depth++)
                {
                    if (depth > 0)
                        stackTrace.Append(Environment.NewLine);
                    var mb = st.GetFrame(i).GetMethod();
                    if (mb is MethodInfo methodInfo)
                        stackTrace.Append(methodInfo.ReturnType.Name + " ");
                    stackTrace.Append((mb.ReflectedType?.FullName ?? "???") + "." + mb.Name);
                    stackTrace.Append(
                        $"({string.Join(", ", mb.GetParameters().Select(p => $"{p.ParameterType.Name} {p.Name}"))})");
                }

                if (_connection is { State: ConnectionState.Open })
                    LogToDatabase(error, buildDate, version, stackTrace.ToString(), level);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error , "Failed to write to DB " + Message(ex));
            }
        }

        private void LogToDatabase(string error, DateTime buildDate, Version version, string stackTrace, LogLevel level)
        {
            using var cmd = new SqlCommand(Query, _connection);
            cmd.Parameters.Add(new SqlParameter("@dt", DateTime.Now));
            cmd.Parameters.Add(new SqlParameter("@msg", error));
            cmd.Parameters.Add(new SqlParameter("@ctxt", AppDomain.CurrentDomain.FriendlyName));
            cmd.Parameters.Add(new SqlParameter("@trace", stackTrace));
            cmd.Parameters.Add(new SqlParameter("@comp", Environment.MachineName));
            cmd.Parameters.Add(new SqlParameter("@user", Environment.UserName));
            cmd.Parameters.Add(new SqlParameter("@bd", buildDate));
            cmd.Parameters.Add(new SqlParameter("@ver", version.ToString()));
            cmd.Parameters.Add(new SqlParameter("@level", LogLevelToString(level)));

            cmd.ExecuteNonQueryAsync();
        }

        private static string LogLevelToString(LogLevel level)
        {
            return level switch
            {
                LogLevel.Debug => LogLevelDebug,
                LogLevel.Information => LogLevelInfo,
                LogLevel.Error => LogLevelError,
                LogLevel.Critical => LogLevelExternalProblem,
                _ => level.ToString()
            };
        }

        private string Message(Exception ex)
        {
            try
            {
                var errText = new StringBuilder();
                // add some information on running process
                errText.Append("process started: " + Process.GetCurrentProcess().StartTime + ", ");
                errText.Append(IntPtr.Size == 4 ? "32 bit" : "64 bit");
                errText.Append(". ");
                errText.Append(ex?.Message);
                ex = ex?.InnerException;

                while (ex != null)
                {
                    errText.Append(Environment.NewLine + ex.Message);
                    ex = ex.InnerException;
                }

                return errText.ToString();
            }
            catch(Exception e)
            {
                _logger.LogWarning(e, "Fail build message from exception");
                var message = ex?.Message ?? string.Empty;
                return message;
            }
        }

        #endregion Private Methods
    }
}