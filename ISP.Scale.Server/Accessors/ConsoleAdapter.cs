﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ISP.Scale.Server.Accessors
{
    public class ConsoleAdapter : IRepositoryAdapter
    {
        public int SaveToRepository(IList<CrossCutting.Scale> scalesToSave)
        {
            foreach (var scale in scalesToSave)
            {
                System.Console.WriteLine($"Save: {scale}");
            }
            return scalesToSave.Count;
        }
    }
}