﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace ISP.Scale.Server.Accessors
{
    public interface IDbLog
    {
        void Log(Exception ex, string error, LogLevel level);
        void Log(string error, LogLevel level);
    }
}