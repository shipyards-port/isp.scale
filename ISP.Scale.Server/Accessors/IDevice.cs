﻿using System;
using ISP.Scale.Server.CrossCutting;

namespace ISP.Scale.Server.Accessors
{
    public interface IDevice
    {
        IObservable<WeightMessage> Progress { get; }

        bool IsConnected { get; }

        string ScaleId { get; }
    }
}