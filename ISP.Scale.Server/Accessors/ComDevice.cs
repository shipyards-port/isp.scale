﻿using System;
using System.IO.Ports;
using System.Reactive.Subjects;
using System.Timers;
using ISP.Scale.Server.CrossCutting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace ISP.Scale.Server.Accessors
{
    public class ComDevice : IDevice, IDisposable
    {
        #region Members

        private readonly IDbLog _dbLogger;
        private readonly Subject<WeightMessage> _progressSubject;
        private SerialPort _serialPort;
        private readonly object _bufferLock = new object();
        private string _inputBuffer = string.Empty;
        private readonly Timer _reconnectTimer;
        private int _reconnectTry = 1;

        #endregion Members

        #region Properties

        public string Port { get; }

        public IObservable<WeightMessage> Progress => _progressSubject;

        public bool IsConnected
        {
            get
            {
                if (_serialPort == null)
                    return false;
                return _serialPort.IsOpen;
            }
        }

        public string ScaleId { get; }

        #endregion Properties

        #region Constructor

        public ComDevice(string scaleId, string port, IServiceProvider provider)
        {
            _dbLogger = provider.GetRequiredService<IDbLog>();
            _progressSubject = new Subject<WeightMessage>();
            ScaleId = scaleId;
            Port = port;

            var configuration = provider.GetRequiredService<IConfiguration>();
            var reconnectIntervalSeconds = configuration.GetSection("ReconnectIntervalSeconds").Get<int>();
            _reconnectTimer = new Timer(reconnectIntervalSeconds * 1000);
            _reconnectTimer.Elapsed += ReconnectTimer_Elapsed;

            _dbLogger.Log($"{nameof(ComDevice)}: created with parameters => ScaleId={ScaleId}, port={Port}", LogLevel.Information);

            if(!string.IsNullOrEmpty(Port))
                Reconnect();
        }

        #endregion Constructor

        #region Public Methods

        public void Dispose()
        {
            _dbLogger.Log($"{nameof(ComDevice)}: scaleId={ScaleId}, port={Port} Disposed", LogLevel.Information);
            _progressSubject?.OnCompleted();
            _progressSubject?.Dispose();
            _serialPort?.Dispose();
        }

        #endregion Public Methods

        #region Private Methods

        private void ReconnectTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Reconnect();
        }

        private void Reconnect()
        {
            var result = Connect();
            if (result)
            {
                _reconnectTimer.Stop();
                _reconnectTry = 1;
            }
            else if (!_reconnectTimer.Enabled)
            {
                _reconnectTry++;
                _dbLogger.Log($"{nameof(ComDevice)}: try reconnect to Port={Port}, reconnect count={_reconnectTry}", LogLevel.Information);
                _reconnectTimer.Start();
            }
        }

        private bool Connect()
        {
            if (_serialPort != null)
            {
                _serialPort?.Close();
                _serialPort?.Dispose();
                _dbLogger.Log($"{nameof(ComDevice)}: scaleId={ScaleId}, port={Port} close connection", LogLevel.Information);
            }
            if (!string.IsNullOrEmpty(Port))
            {
                try
                {
                    _serialPort = new SerialPort(Port, 2400, Parity.None, 8, StopBits.One);
                    _serialPort.Open();
                    _serialPort.RtsEnable = true;
                    _serialPort.BreakState = true;
                    _serialPort.DataReceived += SerialDataReceived;
                    _serialPort.ErrorReceived += SerialPortOnErrorReceived;
                }
                catch (Exception ex)
                {
                    _serialPort = null;
                    _dbLogger.Log(ex, $"{nameof(ComDevice)}: failed connect to serial port. ScaleId={ScaleId}, port={Port}", LogLevel.Critical);
                }
            }

            _dbLogger.Log($"{nameof(ComDevice)}: scaleId={ScaleId}, port={Port} connected={IsConnected}", LogLevel.Information);
            return IsConnected;
        }

        private void SerialPortOnErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            _dbLogger.Log($"{nameof(ComDevice)}: error received. ScaleId={ScaleId}, port={Port}, ErrorType={e.EventType}", LogLevel.Error);
            Reconnect();
        }

        private void SerialDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            lock (_bufferLock)
            {
                try
                {
                    _inputBuffer += _serialPort.ReadExisting();
                }
                catch (Exception ex)
                {
                    _dbLogger.Log(ex, $"{nameof(ComDevice)}: failed read data from ScaleId={ScaleId}, port={Port}", LogLevel.Error);
                    Reconnect();
                    return;
                }

                var msg = string.Empty;
                try
                {
                    msg = GetNextMsg();
                    if (string.IsNullOrEmpty(msg))
                        return;

                    var weightMsg = WeightMessageFactory.FromString(ScaleId, Port, msg, DateTime.Now);
                    _progressSubject.OnNext(weightMsg);
                    //_dbLogger.Log($"{nameof(ComDevice)} Port={Port} receive: {msg} --- {weightMsg}", LogLevel.Trace);
                }
                catch (Exception ex)
                {
                    _dbLogger.Log(ex, $"{nameof(ComDevice)}: failed convert to system message. ScaleId={ScaleId}, port={Port}, msg={msg}", LogLevel.Error);
                }
            }
        }

        private string GetNextMsg()
        {
            if (string.IsNullOrEmpty(_inputBuffer))
                return string.Empty;
            var endIndex = _inputBuffer.LastIndexOf('\r');
            // getting the last weight message for now (previous one's are not relevant)
            if (endIndex < 0 || _inputBuffer.Length < WeightMessageFactory.MessageCharCount+1)
                return string.Empty;

            var response = _inputBuffer.Substring(endIndex- WeightMessageFactory.MessageCharCount, WeightMessageFactory.MessageCharCount);
            _inputBuffer = _inputBuffer.Substring(endIndex + 1);

            return response;
        }

        #endregion Private Methods

    }
}