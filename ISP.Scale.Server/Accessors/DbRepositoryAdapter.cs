﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ISP.Scale.Server.Accessors
{
    public class DbRepositoryAdapter : IRepositoryAdapter
    {
        private readonly IDbLog _dbLogger;
        private readonly SqlConnection _connection;

        public DbRepositoryAdapter(IConfiguration configuration, IDbLog dbLogger)
        {
            _dbLogger = dbLogger;
            var connectionString = configuration.GetConnectionString("PilScalesConnection");
            _dbLogger.Log($"{nameof(DbRepositoryAdapter)}: try created with params {connectionString}", LogLevel.Information);
            _connection = new SqlConnection(connectionString);
            try
            {
                _connection.Open();
                _dbLogger.Log($"{nameof(DbRepositoryAdapter)}: connected", LogLevel.Information);
            }
            catch (Exception e)
            {
                _dbLogger.Log(e, $"{nameof(DbRepositoryAdapter)}: can not connect to remote logger", LogLevel.Warning);
            }
        }

        public int SaveToRepository(IList<CrossCutting.Scale> scalesToSave)
        {
            try
            {
                if (_connection.State == ConnectionState.Broken || _connection.State == ConnectionState.Closed)
                {
                    _dbLogger.Log($"{nameof(DbRepositoryAdapter)}: connection state={_connection.State}, close", LogLevel.Debug);
                    _connection.Close();
                    _dbLogger.Log($"{nameof(DbRepositoryAdapter)}: connection state={_connection.State}, try reopen", LogLevel.Information);
                    _connection.Open();
                }

                var sb = new StringBuilder();

                for (var i = 0; i < scalesToSave.Count; i++)
                {
                    sb.Append("update Weighing " + 
                              $"set Head1=@h1_{i}, Head2=@h2_{i}, WeighingDate=@wdate_{i}, WeighingTime=@wtime_{i}, Status=@st_{i}, InsertTime=@itime_{i} " +
                              $"where ScalesId = @id_{i} " +
                              "if @@ROWCOUNT=0" + "\n" +
                              "insert into Weighing " +
                              "(ScalesId, Head1, Head2, WeighingDate, WeighingTime, Status, InsertTime) " +
                              $"values (@id_{i}, @h1_{i}, @h2_{i}, @wdate_{i}, @wtime_{i}, @st_{i}, @itime_{i});");
                }

                using var cmd = new SqlCommand(sb.ToString(), _connection);
                for (var i = 0; i < scalesToSave.Count; i++)
                {
                    var (scaleId, scales) = scalesToSave[i];

                    var h1 = scales[0];
                    var h2 = scales[1];
                    var latestTime = h1.HeadReceivedTime > h2.HeadReceivedTime
                        ? h1.HeadReceivedTime
                        : h2.HeadReceivedTime;

                    cmd.Parameters.Add(new SqlParameter($"@id_{i}", scaleId));
                    cmd.Parameters.Add(new SqlParameter($"@h1_{i}", h1.HeadWeight));
                    cmd.Parameters.Add(new SqlParameter($"@h2_{i}", h2.HeadWeight));
                    cmd.Parameters.Add(new SqlParameter($"@wdate_{i}", latestTime.Date));
                    cmd.Parameters.Add(new SqlParameter($"@wtime_{i}", latestTime.TimeOfDay));
                    cmd.Parameters.Add(new SqlParameter($"@st_{i}", "OK")); //TODO:
                    cmd.Parameters.Add(new SqlParameter($"@itime_{i}", DateTime.Now)); 

                }
                return cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}