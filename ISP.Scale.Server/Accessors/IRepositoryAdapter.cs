﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ISP.Scale.Server.Accessors
{
    public interface IRepositoryAdapter
    {
        int SaveToRepository(IList<CrossCutting.Scale> scalesToSave);
    }
}