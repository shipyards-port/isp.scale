﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ISP.Scale.Server.Accessors;

namespace ISP.Scale.Server.Engines
{
    public interface IWeightEngine
    {
        void SaveToRepository();
        void SubscribeToEvents(IEnumerable<IDevice> comDevices);
    }
}