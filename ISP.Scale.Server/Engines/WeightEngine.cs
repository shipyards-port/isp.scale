﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ISP.Scale.Server.Accessors;
using ISP.Scale.Server.CrossCutting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ISP.Scale.Server.Engines
{
    public class WeightEngine : IWeightEngine
    {
        #region Members

        private readonly IRepositoryAdapter _repository;
        private readonly IDbLog _dbLogger;
        private readonly ILogger<WeightEngine> _logger;
        private readonly IDictionary<string, (CrossCutting.Scale, bool)> _weightDictionary;
        private readonly int _minimalValueChange;

        #endregion Members

        #region Properties


        #endregion Properties

        #region Constructor

        public WeightEngine(IConfiguration configuration, IRepositoryAdapter repositoryAdapter, IDbLog dbLogger, ILogger<WeightEngine> logger)
        {
            _repository = repositoryAdapter;
            _dbLogger = dbLogger;
            _logger = logger;
            _minimalValueChange = configuration.GetSection("MinimalWeightDelta").Get<int>();

            _weightDictionary = new Dictionary<string, (CrossCutting.Scale, bool)>();
            var comPortsDevices = configuration.GetSection("comPortsDevices").Get<Dictionary<string, object>[]>();

            foreach (var comPortsDevice in comPortsDevices)
            {
                var scaleId = (string)comPortsDevice["ScaleId"];
                var head1Name = (string)comPortsDevice["Head1name"];
                var head1Port = (string)comPortsDevice["Head1Port"];
                var head2Name = (string)comPortsDevice["Head2name"];
                var head2Port = (string)comPortsDevice["Head2Port"];
                
                var devices = new HeadScale[2];
                devices[0] = new HeadScale(scaleId, head1Name, head1Port) { HeadReceivedTime = DateTime.MinValue };
                devices[1] = new HeadScale(scaleId, head2Name, head2Port) { HeadReceivedTime = DateTime.MinValue };
                var scale = new CrossCutting.Scale(scaleId, devices);
                _weightDictionary.TryAdd(scaleId, (scale, false));
            }
            
            _dbLogger.Log($"{nameof(WeightEngine)} created with {_weightDictionary.Count} scales", LogLevel.Information);
        }

        #endregion Constructor

        #region Public Methods

        public void SaveToRepository()
        {
            var scalesToSave = new List<CrossCutting.Scale>();

            //take only dirty scales
            foreach (var (_, (scale, dirty)) in _weightDictionary)
            {
                lock (scale.LockObj)
                {
                    if (dirty)
                    {
                        scalesToSave.Add(scale);
                    }
                }
            }

            var scaleToSaveCount = scalesToSave.Count;
            if (scaleToSaveCount > 0)
            {
                _dbLogger.Log($"{nameof(WeightEngine)} find {scaleToSaveCount} items to save to DB", LogLevel.Debug);
                //remove dirty flag
                foreach (var scale in scalesToSave)
                {
                    lock (scale.LockObj)
                    {
                        _weightDictionary[scale.ScaleId] = (scale, false);
                    }
                }

                try
                {
                    //save to DB
                    var savedCount = _repository.SaveToRepository(scalesToSave);
                    _logger.LogTrace($"Reposetore save {savedCount} items");

                    if (savedCount < 1)
                    {
                        var sb = new StringBuilder();
                        foreach (var scale in scalesToSave)
                        {
                            sb.Append($"{scale}, ");
                        }

                        _dbLogger.Log($"Not all scales saved, scales to save = {sb}", LogLevel.Error);
                    }
                }
                catch (Exception e)
                {
                    _dbLogger.Log(e, $"{nameof(WeightEngine)} save to DB failed", LogLevel.Critical);
                }
            }
        }

        public void SubscribeToEvents(IEnumerable<IDevice> comDevices)
        {
            foreach (var comDevice in comDevices)
            {
                comDevice.Progress.Subscribe(OnDataReceived);
            }
        }

        #endregion Public Methods

        #region Private Methods

        private void OnDataReceived(WeightMessage weightMessage)
        {
            var (scaleId, statuses, newWeight, port, receivedTime) = weightMessage;

            _dbLogger.Log($"{nameof(WeightEngine)} received data {weightMessage}", LogLevel.Trace);

            lock (_weightDictionary[scaleId].Item1.LockObj)
            {
                HeadScale headScale;
                if (_weightDictionary[scaleId].Item1.HeadScales[0].Port == port)
                    headScale = _weightDictionary[scaleId].Item1.HeadScales[0];
                else
                    headScale = _weightDictionary[scaleId].Item1.HeadScales[1];

                //check if update needed, must update first time
                if (headScale.HeadReceivedTime != DateTime.MinValue && Math.Abs(headScale.HeadWeight - newWeight) <= _minimalValueChange)
                    return;

                headScale.HeadWeight = newWeight;
                headScale.HeadReceivedTime = receivedTime;
                var newValue = (_weightDictionary[scaleId].Item1, true); //<new value, dirty flag>

                _dbLogger.Log($"{nameof(WeightEngine)} scale {scaleId} changed to {newValue.Item1}", LogLevel.Debug);
                _weightDictionary[scaleId] = newValue;
            }
        }

        #endregion Private Methods
    }
}