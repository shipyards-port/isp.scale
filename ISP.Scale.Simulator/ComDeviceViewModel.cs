﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Timers;
using System.Windows.Input;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;

namespace ISP.Scale.Simulator
{

    [Flags]
    public enum StatusType
    {
        Normal = 1, // 0=Normal 1=No weight display
        GrossNet = 2, // 0=Gross 1=Net
        AutoZero = 4, // 0=No Auto zero 1=Auto zero
        Range = 8, // 0=Within range 1=Out of range
        Standstill = 16,// 0=No standstill 1=Standstill
        Minimum = 32, // 0=Normal 1=Under minimum weighing range
        Const = 64, // Always 1
        Parity = 128, // Zero or parity
    }

    public class ComDeviceViewModel : ObservableObject
    {
        #region Members

        private const string WeightFormat = "000000";
        private string _port = "2";
        private string _weight = "2000";
        private bool _autoSending;
        private bool _active;
        private List<int> _messageTypes;
        private int _messageType=1;

        private byte _statusBytes;
        private readonly byte[] _outputBytes = new byte[20];
        private readonly SerialPort _serialPort;
        private readonly Timer _sendingTimer;
        private int _timerInterval = 100;
        private bool _randomize = true;
        private readonly Random _random = new();

        #endregion Members

        #region Properties

        public string TimerInterval
        {
            get => _timerInterval.ToString();
            set
            {
                if (int.TryParse(value, out _timerInterval))
                {
                    if(_sendingTimer.Enabled)
                        _sendingTimer.Stop();

                    _sendingTimer.Interval = _timerInterval;

                    if (AutoSending)
                    {
                        _sendingTimer.Start();
                    }
                }
            }
        }

        public string Port
        {
            get => _port;
            set => SetProperty(ref _port, value);
        }

        public string Weight
        {
            get => _weight;
            set => SetProperty(ref _weight, value);
        }

        public bool AutoSending
        {
            get => _autoSending;
            set
            {
                SetProperty(ref _autoSending, value);

                if (value)
                {
                    _sendingTimer.Start();
                }
                else
                {
                    _sendingTimer.Stop();
                }
            }
        }

        public bool Active
        {
            get => _active;
            set => SetProperty(ref _active, value);
        }

        public int MessageType
        {
            get => _messageType;
            set => SetProperty(ref _messageType, value);
        }

        public List<int> MessageTypes
        {
            get => _messageTypes;
            set => SetProperty(ref _messageTypes, value);
        }

        public bool NormalStatus
        {
            get => (_statusBytes & (byte)StatusType.Normal) == (byte)StatusType.Normal;
            set
            {
                if (value != NormalStatus)
                {
                    if (value)
                        _statusBytes += (byte)StatusType.Normal;
                    else
                        _statusBytes -= (byte)StatusType.Normal;
                    OnPropertyChanged();
                }
            }
        }

        public bool GrossNetStatus
        {
            get => (_statusBytes & (byte)StatusType.GrossNet) == (byte)StatusType.GrossNet;
            set
            {
                if(value != GrossNetStatus)
                {
                    if (value)
                        _statusBytes += (byte)StatusType.GrossNet;
                    else
                        _statusBytes -= (byte)StatusType.GrossNet;
                    OnPropertyChanged();
                }
            }
        }

        public bool AutoZeroStatus
        {
            get => (_statusBytes & (byte)StatusType.AutoZero) == (byte)StatusType.AutoZero;
            set
            {
                if (value != AutoZeroStatus)
                {
                    if (value)
                        _statusBytes += (byte)StatusType.AutoZero;
                    else
                        _statusBytes -= (byte)StatusType.AutoZero;
                    OnPropertyChanged();
                }
            }
        }

        public bool RangeStatus
        {
            get => (_statusBytes & (byte)StatusType.Range) == (byte)StatusType.Range;
            set
            {
                if (value != RangeStatus)
                {
                    if (value)
                        _statusBytes += (byte)StatusType.Range;
                    else
                        _statusBytes -= (byte)StatusType.Range;
                    OnPropertyChanged();
                }
            }
        }

        public bool StandstillStatus
        {
            get => (_statusBytes & (byte)StatusType.Standstill) == (byte)StatusType.Standstill;
            set
            {
                if (value != StandstillStatus)
                {
                    if (value)
                        _statusBytes += (byte)StatusType.Standstill;
                    else
                        _statusBytes -= (byte)StatusType.Standstill;
                    OnPropertyChanged();
                }
            }
        }

        public bool MinimumStatus
        {
            get => (_statusBytes & (byte)StatusType.Minimum) == (byte)StatusType.Minimum;
            set
            {
                if (value != MinimumStatus)
                {
                    if (value)
                        _statusBytes += (byte)StatusType.Minimum;
                    else
                        _statusBytes -= (byte)StatusType.Minimum;
                    OnPropertyChanged();
                }
            }
        }

        public bool ConstStatus => (_statusBytes & (byte)StatusType.Const) == (byte)StatusType.Const;

        public bool ParityStatus
        {
            get => (_statusBytes & (byte)StatusType.Parity) == (byte)StatusType.Parity;
            set
            {
                if (value != ParityStatus)
                {
                    if (value)
                        _statusBytes += (byte)StatusType.Parity;
                    else
                        _statusBytes -= (byte)StatusType.Parity;
                    OnPropertyChanged();
                }
            }
        }

        public bool Randomize
        {
            get => _randomize;
            set => SetProperty(ref _randomize, value);
        }

        public ICommand SendCommand { get; }
        public ICommand ConnectCommand { get; }


        #endregion Properties

        #region Constroctur

        public ComDeviceViewModel()
        {
            _serialPort = new SerialPort();
            _serialPort.BaudRate = 2400;
            _serialPort.Parity = Parity.None;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            _serialPort.Handshake = Handshake.None;

            _statusBytes = 64; //bit 6 const
            _messageTypes = new List<int>() { 1, 2 };

            SendCommand = new RelayCommand(SendData);
            ConnectCommand = new RelayCommand(Connect);

            _sendingTimer = new Timer();
            _sendingTimer.Interval = 100;
            _sendingTimer.Elapsed += SendingTimerOnElapsed;
        }

        #endregion Constroctur

        #region Private Methods

        private void SendingTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            SendData();
        }

        private void SendData()
        {
            if (MessageType == 2)
                SendData2();
            else
                SendData1();
        }
        private void SendData1()
        {
            if (!_serialPort.IsOpen)
                Connect();

            if (double.TryParse(Weight, out var weightDouble))
            {
                if (Randomize)
                {
                    weightDouble += _random.Next(-200, 200);
                    if (weightDouble < 0)
                        weightDouble = 0;
                    Weight = weightDouble.ToString();
                }

                _outputBytes[0] = _statusBytes;
                _outputBytes[1] = weightDouble >= 0 ? (byte)'+' : (byte)'-';
                var weightCharArr = Math.Abs(weightDouble).ToString(WeightFormat).ToCharArray();
                _outputBytes[2] = (byte)weightCharArr[0];
                _outputBytes[3] = (byte)weightCharArr[1];
                _outputBytes[4] = (byte)weightCharArr[2];
                _outputBytes[5] = (byte)weightCharArr[3];
                _outputBytes[6] = (byte)weightCharArr[4];
                _outputBytes[7] = (byte)weightCharArr[5];
                _outputBytes[8] = (byte)'\r';

                _serialPort.Write(_outputBytes, 0, 9);
            }
        }

        private void SendData2()
        {
            if (!_serialPort.IsOpen)
                Connect();

            if (double.TryParse(Weight, out var weightDouble))
            {
                if (Randomize)
                {
                    weightDouble += _random.Next(-200, 200);
                    if (weightDouble < 0)
                        weightDouble = 0;
                    Weight = weightDouble.ToString();
                }

                _outputBytes[0] = (byte)'A';
                _outputBytes[1] = (byte)'1';
                _outputBytes[2] = _statusBytes;
                _outputBytes[3] = weightDouble >= 0 ? (byte)'+' : (byte)'-';
                _outputBytes[4] = (byte)' ';
                _outputBytes[5] = (byte)' ';
                var weightCharArr = Math.Abs(weightDouble).ToString(WeightFormat).ToCharArray();
                _outputBytes[6] = (byte)weightCharArr[0];
                _outputBytes[7] = (byte)weightCharArr[1];
                _outputBytes[8] = (byte)weightCharArr[2];
                _outputBytes[9] = (byte)weightCharArr[3];
                _outputBytes[10] = (byte)weightCharArr[4];
                _outputBytes[11] = (byte)weightCharArr[5];
                _outputBytes[12] = (byte)'k';
                _outputBytes[13] = (byte)'g';
                _outputBytes[14] = (byte)'\r';

                _serialPort.Write(_outputBytes, 0, 15);
            }
        }

        private void Connect()
        {
            if (_serialPort.IsOpen)
                _serialPort.Close();
            // Open port
            if (_port != null)
            {
                _serialPort.PortName = $"com{_port}";
                _serialPort.Open();

                _serialPort.RtsEnable = true;
                // _serialPort.BreakState = true;
            }
        }
        

        #endregion Private Methods
    }
}